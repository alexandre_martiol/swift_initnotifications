//
//  AppDelegate.swift
//  InitNotifications
//
//  Created by Alexandre Martinez Olmos on 9/3/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Override point for customization after application launch.
        
        //EXERCISE 1
        //This code allows the app to use notifications with different types.
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes:UIUserNotificationType.Sound |
            UIUserNotificationType.Alert |
            UIUserNotificationType.Badge,
            categories: nil))
        
        //EXERCISE 2
        //Acciones
        var accion1: UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        accion1.identifier = "ACCION_1"
        accion1.title = "Acción 1"
        accion1.activationMode = UIUserNotificationActivationMode.Background
        accion1.destructive = true
        accion1.authenticationRequired = false
        
        var accion2: UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        accion2.identifier = "ACCION_2"
        accion2.title = "Acción 2"
        accion2.activationMode = UIUserNotificationActivationMode.Foreground
        accion2.destructive = false
        accion2.authenticationRequired = false
        
        var accion3: UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        accion3.identifier = "ACCION_3"
        accion3.title = "Acción 3"
        accion3.activationMode = UIUserNotificationActivationMode.Background
        accion3.destructive = true
        accion3.authenticationRequired = false
        
        //Las acciones las debemos agrupar en una categoría
        var miCategoria: UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
        miCategoria.identifier = "CATEGORIA_1"
        
        let defaultActions: NSArray = [accion1, accion2, accion3]
        let minimalActions: NSArray = [accion1, accion2]
        
        miCategoria.setActions(defaultActions as! [AnyObject], forContext: UIUserNotificationActionContext.Default)
        miCategoria.setActions(minimalActions as! [AnyObject], forContext: UIUserNotificationActionContext.Minimal)
        
        //Damos permisos a nuestra app para mostrar notificaciones con la categoría que hemos creado
        let categories: NSSet = NSSet(object: miCategoria)
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: UIUserNotificationType.Sound |
            UIUserNotificationType.Alert |
            UIUserNotificationType.Badge,
            categories: categories as? Set<NSObject>))
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //Notification showed when user close the app to home.
        var notification = UILocalNotification()
        notification.alertBody = "La aplicación se ha cerrado"
        application.presentLocalNotificationNow(notification)
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //EXERCISE 2
    //This method call to delegate in order to execute the actions of the notifications.
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?,forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        
        if(identifier == "ACCION_1"){
            NSNotificationCenter.defaultCenter().postNotificationName("accion1Seleccionada", object: nil)
        }
            
        else if(identifier == "ACCION_2"){
            NSNotificationCenter.defaultCenter().postNotificationName("accion2Seleccionada", object: nil)
        }
        
        else if(identifier == "ACCION_3"){
            NSNotificationCenter.defaultCenter().postNotificationName("accion3Seleccionada", object: nil)
        }
            completionHandler()
    }
}

