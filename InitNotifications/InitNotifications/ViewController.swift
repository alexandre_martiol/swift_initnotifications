//
//  ViewController.swift
//  InitNotifications
//
//  Created by Alexandre Martinez Olmos on 9/3/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //EXERCISE 1
        //Notification showed passed 10 seconds of the load view.
        /*var notification = UILocalNotification()
        notification.fireDate = NSDate().dateByAddingTimeInterval(10)
        notification.alertBody = "Alerta!!!"
        UIApplication.sharedApplication().scheduleLocalNotification(notification)*/
        
        //EXERCISE 2
        var notification = UILocalNotification()
        notification.fireDate = NSDate().dateByAddingTimeInterval(10)
        notification.alertBody = "Hola, soy una notificacion!!!"
        notification.category = "CATEGORIA_1"
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "dibujarRectangulo:", name: "accion1Seleccionada", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "muestraMensaje:", name: "accion2Seleccionada", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "muestraImagen:", name: "accion3Seleccionada", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func dibujarRectangulo(notification: NSNotification) {
        var view: UIView = UIView(frame: CGRectMake(10, 10, 100, 100))
        view.backgroundColor = UIColor.redColor()
        self.view.addSubview(view)
    }
    
    func muestraMensaje(notification: NSNotification){
        var message: UIAlertController = UIAlertController(title: "Mensaje de notificación", message: "Hola qué tal!", preferredStyle: UIAlertControllerStyle.Alert)
        
        message.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(message, animated: true, completion:nil)
    }
    
    func muestraImagen(notification: NSNotification){
        self.imageView.image = UIImage(named: "pokemonFight.jpg")
    }
}

